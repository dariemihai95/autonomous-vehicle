from gpiozero import Motor, AngularServo
import cv2
import picamera
import picamera.array
import numpy as np

motor = Motor(forward = 12, backward = 13, pwm=True)
servo = AngularServo(3, initial_angle=0, min_pulse_width=1.15/1000, 
                     max_pulse_width=1.60/1000, min_angle=-30, max_angle=30)
servo.detach()

with picamera.PiCamera() as camera:
    with picamera.array.PiRGBArray(camera) as stream:
        camera.resolution = (320, 240)
        camera.vflip = True
        camera.hflip = True

        while True:
            camera.capture(stream, 'rgb', use_video_port=True)
            # stream.array now contains the image data in BGR order
            #cv2.imshow('frame', stream.array)

            #here we choose the lower part of the img(intersted in)
            cut_img = stream.array[120:140,:]

            #RGB 2 grayscale
            img = cv2.cvtColor(cut_img, cv2.COLOR_BGR2GRAY)

            #print(img)

            #grayscale to pure Black and white with thresh being where white starts
            (thresh, im_bw) = cv2.threshold(img, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
            thresh = 170
            im_bw = cv2.threshold(img, thresh, 255, cv2.THRESH_BINARY)[1]

            #gaussian filter to smooth out blacks and whites
            blur = cv2.GaussianBlur(im_bw,(5,5),0)

            #AVERAGE all lines into an 1d string(for graph)
            edges_1d = np.average(blur, axis=0)

            #calculate width of the image, it's half size(later use)
            img_width = len(edges_1d)
            middle = img_width/2

            #to find all maximum values in the array
            maximum = [i for i, x in enumerate(edges_1d) if x == max(edges_1d)]

            #in case of having only 1 max in photo
            if maximum[-1] == maximum[0]:
                compensation = middle - maximum[0]
            else:
                compensation = middle - (maximum[0]+ np.round((maximum[-1]-maximum[0])/2))

            print('Compensation is:',compensation)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            # reset the stream before the next capture
            stream.seek(0)
            stream.truncate()

            motor.forward(0.18)

            if compensation > 0 and compensation < 40:
                servo.angle = -compensation/1.4
            elif compensation < 0 and compensation > -40:
                servo.angle = -compensation/1.4
            elif compensation > 40:
                servo.angle = -29
            elif compensation < -40:
                servo.angle = 29
            else:
                servo.angle = 0

    cv2.destroyAllWindows()
